package com.todo.task.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.nhaarman.mockitokotlin2.*
import com.todo.task.CoroutineTestRule
import com.todo.task.data.TodoRepository
import com.todo.task.model.TodoItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

class ToDoViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private fun sleep() {
        Thread.sleep(500)
    }

    @Test
    fun initTest() {
        val repository: TodoRepository = mock()
        val factory: DataSource.Factory<Int, TodoItem> = mock()
        whenever (repository.getTodoList()).thenReturn(factory)
        val viewModel = ToDoViewModel(repository)

        assertTrue(viewModel.noData.get())
        assertEquals("", viewModel.loadingMsg.value)
        assertTrue(viewModel.todoList.value == null)
    }


    @ExperimentalCoroutinesApi
    @Test
    fun getTodoListFromServerTest() {
        val repository: TodoRepository = mock()
        val factory: DataSource.Factory<Int, TodoItem> = mock()
        whenever (repository.getTodoList()).thenReturn(factory)

        val viewModel = ToDoViewModel(repository)
        runBlocking{
            viewModel.getTodoListFromServer()
            sleep()

            verify(repository, times(1)).getTodoListFromServer()
        }
    }
}