package com.todo.task.remote

import com.todo.task.MapperUtil
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ToDoRemoteMapperTest {

    lateinit var mapper: ToDoRemoteMapper

    @Before
    fun setUp() {
        mapper =
            ToDoRemoteMapper()
    }

    @Test
    fun toToDoEntity() {
        val expected = MapperUtil.createToDoEntity()
        val actual = mapper.toToDoEntity(MapperUtil.createToDoRemoteItem())
        assertEquals(expected, actual)
    }

    @Test
    fun toToDoEntityList() {
        val expected = MapperUtil.createToDoEntityList()
        val actual = mapper.toToDoEntity(MapperUtil.createToDoRemoteItemList())
        assertEquals(expected, actual)
    }
}