package com.todo.task

import com.todo.task.data.entity.ToDoEntity
import com.todo.task.model.TodoItem
import com.todo.task.remote.ToDoRemoteItem

object MapperUtil {


    fun createToDoEntity(): ToDoEntity {

        return ToDoEntity(
            id = 1,
            userId = 1,
            title = "delectus aut autem",
            status = false
        )
    }

    fun createToDoRemoteItem(): ToDoRemoteItem {

        return ToDoRemoteItem(
            id = 1,
            userId = 1,
            title = "delectus aut autem",
            completed = false
        )
    }

    fun createToDoEntityList(): List<ToDoEntity> {
        val list = ArrayList<ToDoEntity>()
        for (i in 0 until 10) {
            list.add(
                ToDoEntity(
                    id = 1,
                    userId = 1,
                    title = "delectus aut autem",
                    status = false
                )
            )
        }
        return list
    }

    fun createToDoRemoteItemList(): List<ToDoRemoteItem> {
        val list = ArrayList<ToDoRemoteItem>()
        for (i in 0 until 10) {
            list.add(
                ToDoRemoteItem(
                    id = 1,
                    userId = 1,
                    title = "delectus aut autem",
                    completed = false
                )
            )
        }
        return list
    }


    fun createToDoItem(): TodoItem {

        return TodoItem(
            id = 1,
            userId = 1,
            title = "delectus aut autem",
            completed = false
        )
    }

    fun createTodoItemList(): List<TodoItem> {
        val list = ArrayList<TodoItem>()
        for (i in 0 until 10) {
            list.add(
                TodoItem(
                    id = 1,
                    userId = 1,
                    title = "delectus aut autem",
                    completed = false
                )
            )
        }
        return list
    }
}