package com.todo.task.data

import com.todo.task.MapperUtil
import com.todo.task.remote.ToDoRemoteMapper

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test


class ToDoDataMapperTest{

    lateinit var mapper: ToDoDataMapper

    @Before
    fun setUp() {
        mapper =
            ToDoDataMapper()
    }

    @Test
    fun toTodoItem() {
        val expected = MapperUtil.createToDoEntity()
        val actual = mapper.toToDoItem(MapperUtil.createToDoEntity())
        assertEquals(expected, actual)
    }

    @Test
    fun toTodoItemList() {
        val expected = MapperUtil.createToDoEntityList()
        val actual = mapper.toToDoItem(MapperUtil.createToDoEntity())
        assertEquals(expected, actual)
    }
}