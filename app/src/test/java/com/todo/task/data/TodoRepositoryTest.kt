package com.todo.task.data


import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.todo.task.data.dao.TodoDao
import com.todo.task.data.entity.ToDoEntity
import com.todo.task.model.TodoItem
import com.todo.task.remote.TodoRemote
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class TodoRepositoryTest {


    @ExperimentalCoroutinesApi
    @Test
    fun getTodoListFromServerTest() = runBlocking {
        val todoDao: TodoDao = mock()
        val todoRemote: TodoRemote = mock()
        val mapper: ToDoDataMapper = mock()

        val repository = TodoRepository(todoDao, todoRemote, mapper)

        val entityList = ArrayList<ToDoEntity>()
        entityList.add(ToDoEntity(1, 1, "test", true))

        val list = ArrayList<TodoItem>()
        list.add(TodoItem(1, 1, "test", true))
        whenever(todoRemote.fetchTodoList()).thenReturn(entityList)
        whenever(mapper.toToDoItem(entityList)).thenReturn(list)

        repository.getTodoListFromServer()
        assertEquals(1, list.size)
    }
}