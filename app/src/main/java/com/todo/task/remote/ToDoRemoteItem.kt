package com.todo.task.remote

data class ToDoRemoteItem(
    var userId: Int = -1,
    var id: Int = -1,
    var title: String = "",
    var completed: Boolean = false
)