package com.todo.task.remote

import com.todo.task.data.entity.ToDoEntity

interface TodoRemote {

    suspend fun fetchTodoList() : List<ToDoEntity>?

}