package com.todo.task.remote

import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface TodoApi {

    @GET("todos")
    fun getTodoList(): Deferred<List<ToDoRemoteItem>>
}