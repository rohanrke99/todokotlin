package com.todo.task.remote

import android.util.Log
import com.todo.task.data.entity.ToDoEntity
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi

const val TAG = "TodoRemote"

class ToDoRemoteImpl(val todoApi: TodoApi, val mapper: ToDoRemoteMapper) : TodoRemote {

    override suspend fun fetchTodoList(): List<ToDoEntity>? {
        val list = getTodoList()
        return mapper.toToDoEntity(list)
    }

    @ExperimentalCoroutinesApi
    open suspend fun getTodoList(): List<ToDoRemoteItem> {
        val request = todoApi.getTodoList()
        val response = executeTodoCall(request)
        return if (response is List<*>) {
            response as List<ToDoRemoteItem>
        } else
            arrayListOf()
    }

    @ExperimentalCoroutinesApi
    private suspend fun executeTodoCall(call: Deferred<List<ToDoRemoteItem>>) = try {
        call.join()
        call.getCompleted()
    } catch (ex: Exception) {
        Log.e(TAG, "Exception during network operation -> ${ex.printStackTrace()}")
    }


}