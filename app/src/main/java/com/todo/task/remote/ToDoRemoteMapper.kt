package com.todo.task.remote

import com.todo.task.data.entity.ToDoEntity

class ToDoRemoteMapper {

    fun toToDoEntity(remoteItem: ToDoRemoteItem): ToDoEntity {
        val en = ToDoEntity()
        remoteItem.apply {
            en.id = this.id
            en.userId = this.userId
            en.title = this.title
            en.status = this.completed
        }
        return en
    }

    fun toToDoEntity(remoteItemList: List<ToDoRemoteItem>): List<ToDoEntity>? {

        if (remoteItemList.isEmpty()) {
            return emptyList()
        }
        return remoteItemList.flatMap { data ->
            listOf(
                ToDoEntity(
                    id = data.id,
                    userId = data.userId,
                    title = data.title,
                    status = data.completed
                )

            )
        }
    }
}