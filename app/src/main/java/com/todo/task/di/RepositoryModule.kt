package com.todo.task.di

import com.todo.task.data.TodoRepository
import org.koin.dsl.module.module

/**
 * Module will initialize all repositories.
 * @author Sumit Lakra
 * @date 12/07/19
 */
internal val repositoryModule = module {

    single { TodoRepository(dao = get(), remote = get(), mapper = get()) }
}