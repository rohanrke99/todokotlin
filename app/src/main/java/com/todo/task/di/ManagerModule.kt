package com.todo.task.di

import com.todo.task.data.ToDoDataMapper
import com.todo.task.remote.ToDoRemoteMapper
import org.koin.dsl.module.module

/**
 * Module to initialize ApiManager.
 * @author Sumit Lakra
 * @date 12/07/19
 */
internal val managerModule = module {

    single { ToDoDataMapper() }
    single { ToDoRemoteMapper() }
}