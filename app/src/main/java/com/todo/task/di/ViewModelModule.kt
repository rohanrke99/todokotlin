package com.todo.task.di

import com.todo.task.ui.ToDoViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * Module will initialize all viewModels.
 */
internal val viewModelModule = module {

    viewModel { ToDoViewModel(repository = get()) }
}