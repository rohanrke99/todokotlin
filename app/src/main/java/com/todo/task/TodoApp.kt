package com.todo.task

import android.app.Application
import com.todo.task.di.appModule
import org.koin.android.ext.android.startKoin

class TodoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin(this, appModule)
    }
}