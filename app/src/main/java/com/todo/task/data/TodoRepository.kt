package com.todo.task.data

import androidx.paging.DataSource
import com.todo.task.data.dao.TodoDao
import com.todo.task.model.TodoItem
import com.todo.task.remote.TodoRemote
import kotlinx.coroutines.ExperimentalCoroutinesApi

open
class TodoRepository(val dao: TodoDao, val remote: TodoRemote, val mapper: ToDoDataMapper) {

    /**
     * Method to get all todos.
     * @return [DataSource.Factory]
     */
    fun getTodoList(): DataSource.Factory<Int, TodoItem> =
        dao.getAllTodos().map { mapper.toToDoItem(it) }

    @ExperimentalCoroutinesApi
    suspend fun getTodoListFromServer() {
        val list = remote.fetchTodoList()
        list?.let {
            dao.insertAll(it)
        }
    }
}