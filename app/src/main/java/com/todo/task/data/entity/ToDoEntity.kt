package com.todo.task.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity class for to_do items.
 */
@Entity(tableName = "todo_table")
data class ToDoEntity(

    @PrimaryKey
    var id: Int = 0,

    @ColumnInfo(name = "user_id")
    var userId: Int = 0,

    var title: String = "",

    var status: Boolean = false
)