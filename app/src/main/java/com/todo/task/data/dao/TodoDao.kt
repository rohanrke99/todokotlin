package com.todo.task.data.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.todo.task.data.entity.ToDoEntity

/**
 * Dao class for todo_table.
 */
@Dao
abstract class TodoDao : RoomBaseDao<ToDoEntity>() {

    @Query("SELECT * FROM todo_table ORDER BY user_id ASC")
    abstract fun getAllTodos(): DataSource.Factory<Int, ToDoEntity>

    @Query("DELETE FROM todo_table")
    abstract fun deleteAll()

    @Query("SELECT count(*) FROM todo_table")
    abstract fun getCount(): Int
}