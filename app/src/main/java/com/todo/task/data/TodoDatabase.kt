package com.todo.task.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.todo.task.data.dao.TodoDao
import com.todo.task.data.entity.ToDoEntity

/**
 * This class represents the RoomDatabase class from android. It contains abstract methods to
 * to get the Dao objects corresponding to each table . see DataBaseModule class for getting the instance
 * of this class.
 */
@Database(
    entities =[
        ToDoEntity::class
    ], version = 1, exportSchema = false
)
abstract class TodoDatabase: RoomDatabase() {

    /**
     * returns to_do dao instance.
     * @return [TodoDao]
     */
    abstract fun todoDao(): TodoDao
}