package com.todo.task.data

import com.todo.task.data.entity.ToDoEntity
import com.todo.task.model.TodoItem

class ToDoDataMapper {

    fun toToDoItem(entity: ToDoEntity): TodoItem {
        val en = TodoItem()
        entity.apply {
            en.id = this.id
            en.userId = this.userId
            en.title = this.title
            en.completed = this.status
        }
        return en
    }

    fun toToDoItem(entityList: List<ToDoEntity>): List<TodoItem>? {

        if (entityList.isEmpty()) {
            return emptyList()
        }
        return entityList.flatMap { data ->
            listOf(
                TodoItem(
                    id = data.id,
                    userId = data.userId,
                    title = data.title,
                    completed = data.status
                )
            )
        }
    }
}