package com.todo.task.base

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.todo.task.R

class WaitingDialogHelper {

    private var dlg: AlertDialog? = null
    private val isShowing: Boolean
        get() = dlg?.isShowing == true

    /**
     * Function to show dialog
     * @param [context] Context
     * @param [title] Int
     */
    fun show(context: Context, title: Int) {
        hideDialog()
        val body = LayoutInflater.from(context).inflate(R.layout.loading, null, false)
        val builder = AlertDialog.Builder(context)
            .setTitle(title)
            .setView(body)
            .setCancelable(true)
        dlg = builder.show()
    }

    /**
     * Function to show dialog
     * @param [context] Context
     * @param [title] String
     */
    fun show(context: Context, title: String) {
        hideDialog()
        val body = LayoutInflater.from(context).inflate(R.layout.loading, null, false)
        val builder = AlertDialog.Builder(context)
            .setTitle(title)
            .setView(body)
            .setCancelable(true)
        dlg = builder.show()
    }

    /**
     * Function to hide dialog
     */
    fun hideDialog() {
        if (isShowing) {
            dlg?.dismiss()
            dlg = null
        }
    }
}