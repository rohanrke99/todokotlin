package com.todo.task.ui

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.todo.task.base.BaseViewModel
import com.todo.task.data.TodoRepository
import com.todo.task.model.TodoItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

private const val PAGE_SIZE = 10
private const val INITIAL_LOAD_SIZE_HINT = 20

class ToDoViewModel(
    val repository: TodoRepository
) : BaseViewModel() {


    val noData = ObservableBoolean(true)
    val loadingMsg = MutableLiveData<String>("")

    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setInitialLoadSizeHint(INITIAL_LOAD_SIZE_HINT)
        .setPageSize(PAGE_SIZE)
        .build()

    val todoList = LivePagedListBuilder<Int, TodoItem>(
        repository.getTodoList(), pagedListConfig
    ).build()

    @ExperimentalCoroutinesApi
    fun getTodoListFromServer() {
        loadingMsg.value = "Fetching List.."
        CoroutineScope(Dispatchers.IO).launch {
            repository.getTodoListFromServer()
        }
    }
}