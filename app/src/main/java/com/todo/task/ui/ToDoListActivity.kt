package com.todo.task.ui

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.todo.task.BR
import com.todo.task.R
import com.todo.task.base.BaseActivity
import com.todo.task.databinding.ActivityTodoListBinding
import com.todo.task.model.TodoItem
import kotlinx.android.synthetic.main.activity_todo_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ToDoListActivity : BaseActivity<ActivityTodoListBinding, ToDoViewModel>() {

    val todoViewModel: ToDoViewModel by viewModel()

    private lateinit var adapter: TodoListAdapter

    override fun getLayoutId(): Int = R.layout.activity_todo_list

    override fun getViewModel(): ToDoViewModel = todoViewModel

    override fun getBindingVariable(): Int = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setActionBar()
        adapter = TodoListAdapter(this)
        setupRecyclerView()
        subscribeObservers()
    }

    private fun setActionBar() {
        supportActionBar?.title = "Todo List"
    }

    private fun setupRecyclerView() {
        rv_todo.layoutManager = LinearLayoutManager(this)
        rv_todo.adapter = adapter
        rv_todo.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
    }

    private fun subscribeObservers() {
        todoViewModel.todoList.observe(this, Observer {todoList ->
            renderList(todoList)
        })

        todoViewModel.loadingMsg.observe(this, Observer { msg->
            if (msg != null) {
                showWaitingDialog(msg)
            }else{
                hideWaitingDialog()
            }
        })
    }

    private fun renderList(pagedTodoList: PagedList<TodoItem>) {
        adapter.submitList(pagedTodoList)
        if (pagedTodoList.isEmpty()) {
            todoViewModel.noData.set(true)
            todoViewModel.getTodoListFromServer()
        }
        else {
            todoViewModel.noData.set(false)
            todoViewModel.loadingMsg.value = null
        }

    }
}
