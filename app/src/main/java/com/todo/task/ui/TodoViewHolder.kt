package com.todo.task.ui

import androidx.recyclerview.widget.RecyclerView
import com.todo.task.BR
import com.todo.task.databinding.TodoItemBinding
import com.todo.task.model.TodoItem


class TodoViewHolder(private val binding: TodoItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(data: TodoItem?) {
        binding.setVariable(BR.todoItem, data)
        binding.executePendingBindings()
    }
}