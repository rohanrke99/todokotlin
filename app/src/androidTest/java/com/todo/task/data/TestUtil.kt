package com.todo.task.data

import androidx.test.platform.app.InstrumentationRegistry
import com.todo.task.data.entity.ToDoEntity
import okhttp3.mockwebserver.MockResponse

object TestUtil {


    fun createToDoEntity(): ToDoEntity {

        return ToDoEntity(
            id = 1,
            userId = 1,
            title = "delectus aut autem",
            status = false
        )
    }

    fun createToDoEntityList(): List<ToDoEntity> {
        val list = ArrayList<ToDoEntity>()
        for (i in 0 until 10) {
            list.add(
                ToDoEntity(
                    id = 1,
                    userId = 1,
                    title = "delectus aut autem",
                    status = false
                )
            )
        }
        return list
    }

    const val MOCK_PORT: Int = 8888

    private fun loadJson(path: String): String {
        val context = InstrumentationRegistry.getInstrumentation().context
        val stream = context.resources.assets.open(path)
        val reader = stream.bufferedReader()
        val stringBuilder = StringBuilder()
        reader.lines().forEach {
            stringBuilder.append(it)
        }
        return stringBuilder.toString()
    }

    fun createResponse(path: String): MockResponse = MockResponse()
        .setResponseCode(200)
        .setBody(loadJson(path))
}